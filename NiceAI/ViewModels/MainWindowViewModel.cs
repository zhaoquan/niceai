using System.Collections.ObjectModel;
using CommunityToolkit.Mvvm.ComponentModel;

namespace NiceAI.ViewModels
{
    public partial class MainWindowViewModel : ObservableObject
    {
        // AI模型集合
        [ObservableProperty]
        private ObservableCollection<AIModelViewModel> _aiModels = new();

        // 当前选中的AI模型
        [ObservableProperty]
        private AIModelViewModel? _selectedModel;

        // 雷达图视图
        [ObservableProperty]
        private object? _radarChartView;

        public MainWindowViewModel()
        {
            // 初始化数据
            LoadAIModels();
            InitializeRadarChart();
        }

        private void LoadAIModels()
        {
            // 从数据源加载AI模型数据
            // 这里应该调用业务逻辑层的方法
            AiModels.Add(new AIModelViewModel { Name = "模型1", OverallScore = 85 });
            AiModels.Add(new AIModelViewModel { Name = "模型2", OverallScore = 90 });
            // 添加更多模型...
        }

        private void InitializeRadarChart()
        {
            // 初始化雷达图
            RadarChartView = new Controls.RadarChart { Models = AiModels };
        }

        // 当选中的模型改变时触发
        partial void OnSelectedModelChanged(AIModelViewModel? value)
        {
            // 当选中的模型改变时，更新雷达图
            if (value != null && RadarChartView is Controls.RadarChart radarChart)
            {
                radarChart.HighlightedModel = value;
            }
        }
    }
}