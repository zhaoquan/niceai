using CommunityToolkit.Mvvm.ComponentModel;

namespace NiceAI.ViewModels
{
    public partial class AIModelViewModel : ObservableObject
    {
        // AI模型的名称
        [ObservableProperty]
        private string _name = "";

        // AI模型的总体得分
        [ObservableProperty]
        private double _overallScore;

        // 可以在这里添加其他属性，如各个维度的得分
    }
}