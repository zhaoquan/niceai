using Avalonia.Controls;
using NiceAI.ViewModels;

namespace NiceAI.Views
{
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            DataContext = new MainWindowViewModel();
        }
    }
}