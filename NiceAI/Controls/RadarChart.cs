using Avalonia;
using Avalonia.Controls;
using Avalonia.Media;
using NiceAI.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;

namespace NiceAI.Controls
{
    public class RadarChart : Control
    {
        public static readonly StyledProperty<IEnumerable<AIModelViewModel>> ModelsProperty =
            AvaloniaProperty.Register<RadarChart, IEnumerable<AIModelViewModel>>(nameof(Models));

        public static readonly StyledProperty<AIModelViewModel?> HighlightedModelProperty =
            AvaloniaProperty.Register<RadarChart, AIModelViewModel?>(nameof(HighlightedModel));

        public IEnumerable<AIModelViewModel> Models
        {
            get => GetValue(ModelsProperty);
            set => SetValue(ModelsProperty, value);
        }

        public AIModelViewModel? HighlightedModel
        {
            get => GetValue(HighlightedModelProperty);
            set => SetValue(HighlightedModelProperty, value);
        }

        public override void Render(DrawingContext context)
        {
            base.Render(context);

            if (Models == null || !Models.Any())
                return;

            var center = new Point(Bounds.Width / 2, Bounds.Height / 2);
            var radius = Math.Min(Bounds.Width, Bounds.Height) / 2 - 10;

            // 绘制雷达图轴
            DrawAxes(context, center, radius);

            // 绘制每个模型的数据
            foreach (var model in Models)
            {
                DrawModelData(context, model, center, radius, model == HighlightedModel);
            }
        }

        private void DrawAxes(DrawingContext context, Point center, double radius)
        {
            // 实现绘制雷达图轴的逻辑
        }

        private void DrawModelData(DrawingContext context, AIModelViewModel model, Point center, double radius, bool isHighlighted)
        {
            // 实现绘制模型数据的逻辑
        }
    }
}